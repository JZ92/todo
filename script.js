const addButton = document.querySelector(".addButton");
const showAllButton = document.querySelector(".showAllBtn");
const showCompletedButton = document.querySelector(".showCompletedBtn");
const showUncompletedButton = document.querySelector(".showUncompletedBtn");
const input = document.querySelector(".inputField input");
const list = document.querySelector(".itemsList");

const handleAdd = () => {
  let tasksList = [];
  let task = { text: input.value, isCompleted: false };
  let currentData = localStorage.getItem("Tasks");
  if (currentData) tasksList = JSON.parse(currentData);
  tasksList.push(task);
  localStorage.setItem("Tasks", JSON.stringify(tasksList));
  input.value = "";
  displayItems(tasksList);
};
addButton.onclick = handleAdd;

const handleShowAll = () => {
  let tasksList = parseData();
  displayItems(tasksList);
};
showAllButton.onclick = handleShowAll;

const handleShowCompleted = () => {
  let tasksList = parseData().filter((item) => item.isCompleted);
  displayItems(tasksList);
};
showCompletedButton.onclick = handleShowCompleted;

const handleShowUncompleted = () => {
  let tasksList = parseData().filter((item) => !item.isCompleted);
  displayItems(tasksList);
};
showUncompletedButton.onclick = handleShowUncompleted;

const removeItem = (text) => {
  let currentData = JSON.parse(localStorage.getItem("Tasks"));
  let newArr = currentData.filter((currItem) => currItem.text !== text);
  localStorage.setItem("Tasks", JSON.stringify(newArr));
  displayItems(newArr);
};

const handleComplete = (index) => {
  let tasksList;
  let currentData = localStorage.getItem("Tasks");
  let currParsedData = JSON.parse(currentData);
  currParsedData[index]["isCompleted"] = true;
  localStorage.setItem("Tasks", JSON.stringify(currParsedData));
  displayItems(currParsedData);
};
const displayItems = (tasksList) => {
  list.innerHTML = "";
  let stringToAppend = "";
  if (tasksList && tasksList.length > 0) {
    tasksList.forEach(
      (item, index) =>
        (stringToAppend += `<li class="item">
            <span>${item.text}</span>
            <span class="delete fas fa-trash fa-2x" onclick="removeItem('${item.text.trim()}')"></span>
            <span class="complete fas fa-check-square fa-2x" onclick="handleComplete(${index})"></span>
            </li>`)
    );
    list.innerHTML = stringToAppend;
  }
};

const parseData = () => {
  let tasksList;
  let currentData = localStorage.getItem("Tasks");
  if (currentData) tasksList = JSON.parse(currentData);
  else tasksList = [];
  return tasksList;
};

displayItems(parseData());
